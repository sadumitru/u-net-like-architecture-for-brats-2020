# requirements.txt

# Core libraries
numpy
pandas
seaborn
matplotlib

# Image processing libraries
opencv-python
Pillow
scikit-image

# Machine learning and deep learning libraries
nilearn
nibabel
keras
tensorflow

# Additional libraries
scikit-learn

# Miscellaneous
nose

# For plotting models
pydot
