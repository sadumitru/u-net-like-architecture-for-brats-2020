from unet import build_unet
from metrics import *
from tensorflow.keras.layers.experimental import preprocessing
from tensorflow.keras.callbacks import (
    ModelCheckpoint,
    ReduceLROnPlateau,
    EarlyStopping,
    TensorBoard,
)
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.layers import Input
from tensorflow.keras.metrics import MeanIoU
from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from tensorflow.keras.utils import plot_model
import tensorflow as tf
from keras.callbacks import CSVLogger
import keras.backend as K
import keras
import nilearn.plotting as nlplt
import nilearn as nl
from PIL import Image, ImageOps
from skimage.transform import resize
from skimage.transform import rotate
import skimage.transform as skTrans
from skimage.util import montage
from skimage import data
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
import cv2
import glob
import shutil
import os
import warnings
from data_generator import DataGenerator
from utils import *

warnings.simplefilter("ignore")


TRAIN_DATASET_PATH = "/home/sorinaau/uni/brats/data/BraTS2020_TrainingData/MICCAI_BraTS2020_TrainingData"
VALIDATION_DATASET_PATH = "/home/sorinaau/uni/brats/data/BraTS2020_ValidationData/MICCAI_BraTS2020_ValidationData"
SEGMENT_CLASSES = {0: "NOT tumor",
                   1: "NECROTIC/CORE", 2: "EDEMA", 3: "ENHANCING"}
IMG_SIZE = 128


input_layer = Input((IMG_SIZE, IMG_SIZE, 2))
model = build_unet(input_layer, "he_normal", 0.2)
model.compile(
    loss="categorical_crossentropy",
    optimizer=Adam(learning_rate=0.001),
    metrics=[
        "accuracy",
        MeanIoU(num_classes=4),
        dice_coef,
        precision,
        sensitivity,
        specificity,
        dice_coef_necrotic,
        dice_coef_edema,
        dice_coef_enhancing,
    ],
)

# plot_model(model,
#            show_shapes=True,
#            show_dtype=False,
#            show_layer_names=True,
#            rankdir='TB',
#            expand_nested=False,
#            dpi=70)

train_and_val_directories = [
    f.path for f in os.scandir(TRAIN_DATASET_PATH) if f.is_dir()]
train_and_test_ids = pathListIntoIds(train_and_val_directories)
train_test_ids, val_ids = train_test_split(train_and_test_ids, test_size=0.2)
train_ids, test_ids = train_test_split(train_test_ids, test_size=0.15)

training_generator = DataGenerator(train_ids)
valid_generator = DataGenerator(val_ids)
test_generator = DataGenerator(test_ids)

showDataLayout(train_ids, val_ids, test_ids)

csv_logger = CSVLogger('training.log', separator=',', append=False)
callbacks = [
    keras.callbacks.EarlyStopping(monitor='loss', min_delta=0,
                                  patience=2, verbose=1, mode='auto'),
    keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.2,
                                      patience=2, min_lr=0.000001, verbose=1),
    keras.callbacks.ModelCheckpoint(filepath='model_.{epoch:02d}-{val_loss:.6f}.m5',
                                    verbose=1, save_best_only=True, save_weights_only=True),
    csv_logger
]

history = model.fit(training_generator,
                    epochs=35,
                    steps_per_epoch=len(train_ids),
                    callbacks=callbacks,
                    validation_data=valid_generator
                    )
model.save("unet_35_epochs.h5")
