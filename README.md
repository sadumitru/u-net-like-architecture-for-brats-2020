# U-Net like architecture for image semantic segmentation BRATS 2020

## Install requirements
```
cd dir_name
pip install -r requirements.txt
```

## Run the code
To run the code the user must download the BRATS 2020 dataset from the official website: https://www.med.upenn.edu/cbica/brats2020/data.html.

The next step is to run the main.py file using python
```
python3 main.py
```